const { response } = require('express')
const express = require('express')
const router = express.Router()
const users = []

router.use((request, response, next) => {
    console.log('route level middleware')
    next()
})

router.get('/register', (request,response) => {
    response.render('register')
})

router.post('/register', (request, response) =>{
    const {email, password} = request.body

    users.push({email, password})
    console.log(users)
    response.redirect('/')
})

module.exports = router