const fs = require('fs')
const { response } = require('express')
const express = require('express')
const app = express()
const port = 3000

app.use(express.urlencoded({extended: false}))
app.set('view engine', 'ejs')

app.use(express.static('public'))
app.use(morgan('dev'))
app.use((request, response, next) => {
    console.log(`${request.method} ${request.url}`)
    next()
})

app.get("/mainpage", function(request, response){
    response.writeHead(200, {'Content-Type' : 'text/html'})
    fs.readFile("./public/chapter-3/index.html")
})

app.get("/rock-paper-scissors", function(request, response){
    response.writeHead(200, {'Content-Type' : 'text/html'})
    fs.readFile("./public/chapter-4/index.html")
})